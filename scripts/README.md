# Скрипты

1. aws_sync.sh - скрипт для синхронизации s3 с папкой на машине
2. build_push.sh - сборка образа, его тегирование и отправка в registry
3. restart_services.sh - рестарт сервисов на машинах
4. update_images.sh - pull образов на всех машинах