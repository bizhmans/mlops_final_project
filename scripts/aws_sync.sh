#!/bin/bash

for host in ${NODES//,/ }
do
    ssh root@$host "aws s3 --endpoint-url ${S3_ENDPOINT} --profile mlops sync s3://final-project/ /var/artifacts/ --delete"
done || exit 1