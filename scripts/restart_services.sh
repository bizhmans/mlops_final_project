#!/bin/bash


hosts=$(docker stack services stack --format "{{.Name}}")
scale_down_str=()
scale_up_str=()
for host in $hosts
do
    scale_down_str+=($host=0)
    scale_up_str+=($host=1)
done || exit 1

docker service scale ${scale_down_str[*]}
docker service scale ${scale_up_str[*]}