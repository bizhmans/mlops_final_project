#!/bin/bash

for host in ${NODES//,/ }
do
    ssh root@$host "docker pull ${REGISTRY_HOST}:${REGISTRY_PORT}/balancer"
    ssh root@$host "docker pull ${REGISTRY_HOST}:${REGISTRY_PORT}/search_worker"
done || exit 1
