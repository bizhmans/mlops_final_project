#!/bin/bash

docker build . -t ${IMAGE_NAME}
docker tag ${IMAGE_NAME} ${REGISTRY_HOST}:${REGISTRY_PORT}/${IMAGE_NAME}
docker push ${REGISTRY_HOST}:${REGISTRY_PORT}/${IMAGE_NAME}