# Balancer
Балансирует трафик между search_worker, каждый из которых отвечает за собственный набор кластеров (1..n).
Сервис при получении запроса имитирует отправку запроса на подсчет эмбединга, затем после получения считает
ближайший кластер (по центрам кластеров) и отправляет в нужный search_worker запрос на поиск ближайших соседей.