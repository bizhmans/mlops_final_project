from fastapi import FastAPI
import typing as tp
import numpy as np
from pickle import load as pload
from json import load as jload
from loguru import logger
import httpx
import os


app = FastAPI()

# центры кластеров, которые даются в условии задания
cluster_centers = None
# размер эмбединга
emb_size = None
# url воркеров, на которые будут отсылаться запросы
worker_gws = None
# сколько соседей хотим запросить
n_candidates = None
# маппинг cluster -> worker_id из worker_gws, все это определяется заранее на этапе разработки
# то есть мы будем обращаться к worker_gws[service_clusters[cluster_id]] воркеру
# например в докер файле есть search_worker1, search_worker2
# заранее выделяем конкретные кластеры под каждый воркер и дальше разрабатываем по этой схеме
# например первые 5 кластеров будут уходить в search_worker_1 и следующие 5 в search_worker2
service_clusters = None


def get_env(env_name: str) -> str:
    val = os.getenv(env_name)
    if val is None:
        raise ValueError(f"{env_name} env is not defined")
    return val


@app.on_event("startup")
def load_cluster_centers() -> tp.Dict[str, np.ndarray]:
    """
    Смотрим на переменные среды и загружаем данные по путям из переменных
    """
    global cluster_centers, emb_size, worker_gws, n_candidates, service_clusters

    worker_gws = list(filter(lambda x: x != "", map(lambda x: x.strip(), get_env("WORKER_GW").split(','))))
    n_candidates = get_env("N_CANDIDATES")
    cluster_centers_path = get_env("CLUSTER_CENTERS_PATH")
    service_clusters_path = get_env("SERVICE_CLUSTERS_PATH")

    logger.info(f"Loading service cluster mappings from {service_clusters_path}")
    with open(service_clusters_path, "rb") as f:
        service_clusters =  {int(k): v for k, v in jload(f).items()}

    logger.info(f"Loading cluster centers from {cluster_centers_path}")
    with open(cluster_centers_path, "rb") as f:
        cluster_centers =  pload(f)
    emb_size = next(iter(cluster_centers.values())).shape[0]
    logger.info(f"Loaded cluster centers from {cluster_centers_path}")


def embed_request(text: str) -> np.ndarray: 
    """
    В реальной системе это был бы async запрос на подсчет эмбедингов, тут же можем обойтись рандомом тк
    в условии написано, что является необязательным пунктом
    """
    return np.random.rand(emb_size)


def find_closest_cluster(embedding: np.ndarray) -> int:
    """
    Ищем среди центров кластеров самый ближайший и возвращаем его id.
    """
    nearest_cluster = None
    nearest_distance = 1e9
    for cluster, center in cluster_centers.items():
        center_dist = np.linalg.norm(center - embedding)
        if center_dist < nearest_distance:
            nearest_distance = center_dist
            nearest_cluster = cluster
    return int(nearest_cluster)


async def query_knn(cluster_id: int) -> tp.List[str]:
    """
    Получив кластер, далее выбираем правильного воркера и отправляем запрос на knn/ann
    """
    worker = worker_gws[service_clusters[cluster_id]]
    logger.info(f"Querying {worker} for {cluster_id} cluster")
    async with httpx.AsyncClient() as client:
        candidates = await client.get(url=f"http://{worker}/knn/", params={
            "cluster_id": cluster_id,
            "n_candidates": n_candidates,
        })
    return candidates.json()["candidates"]


@app.get("/health")
async def healthcheck():
    return {"status": "ok"}


@app.get("/get_candidates/")
async def get_candidates(text: str):
    """
    Главный запрос сервиса, приходит текст, который эмбедится моделью, далее ищется ближайший кластер 
    и запрашивается поиск по кластеру.
    """
    embedding= embed_request(text=text)
    nearest_cluster = find_closest_cluster(embedding=embedding)
    logger.info(f"Nearest cluster == {nearest_cluster}")
    candidates = await query_knn(cluster_id=nearest_cluster)
    logger.info(f"Recieved {len(candidates)} candidates")
    return {
        "candidates": candidates
    }
