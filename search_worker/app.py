from fastapi import FastAPI
from loguru import logger
from json import load
from random import sample

import os


app = FastAPI()
cluster_texts = None


@app.on_event("startup")
def load_data() -> None:
    """
    Загрузка данных в каком кластере лежит какой текст, задается через env путь к файлу
    """
    global cluster_texts

    cluster_texts_path = os.getenv("CLUSTER_TEXTS_PATH")
    if cluster_texts_path is None:
        raise ValueError("CLUSTER_TEXTS_PATH env is not defined")

    logger.info(f"Loading cluster_texts from {cluster_texts_path}")
    with open(cluster_texts_path, "r") as f:
        cluster_texts = load(f)
    logger.info(f"Loaded cluster_texts from {cluster_texts_path}")


@app.get("/knn/")
def knn(cluster_id: str, n_candidates: int):
    """
    по заданию эмбединги необязательны по заданию, поэтому будем просто симулировать поиск по индексу 
    с помощью рандома. Берем случайный текст из кластера, которому принадлежит запрос
    """
    # embedding = np.asarray(embedding) 
    # nearest_texts = find_nearest(...)
    return {
        "candidates": sample(cluster_texts[cluster_id], n_candidates)
    }


@app.get("/health")
async def healthcheck():
    """
    используется в докере в качестве healthcheck
    """
    return {"status": "ok"}
