import pickle
import json
from typing import Dict
from toolz import keymap
import os
from s3fs import S3FileSystem
import click


def get_s3_storage_options():
    s3_client_params = {"endpoint_url": os.getenv("AWS_ENDPOINT_URL")}
    s3_config_params = {"read_timeout": int(os.environ.get("S3_DELAY", 360))}
    s3_storage_options = {
        "key": os.getenv("AWS_ACCESS_KEY_ID"),
        "secret": os.getenv("AWS_SECRET_ACCESS_KEY"),
        "client_kwargs": s3_client_params,
        "config_kwargs": s3_config_params,
    }

    return s3_storage_options


def get_s3_file_system() -> S3FileSystem:
    s3_storage_options = get_s3_storage_options()
    return S3FileSystem(
        anon=False,
        key=s3_storage_options["key"],
        secret=s3_storage_options["secret"],
        client_kwargs=s3_storage_options["client_kwargs"],
    )

@click.command()
@click.option("--output-dir", type=str, default="s3://final-project")
def main(output_dir: str):
    with open("../data/raw/clusters_use_dg1.json", "r") as f:
        first_cluster_texts = json.load(f)
    with open("../data/raw/clusters_use_dg2.json", "r") as f:
        second_cluster_texts = json.load(f)
    print("BEFORE:")

    print(first_cluster_texts.keys())
    print(second_cluster_texts.keys())
    
    with open("../data/raw/clusters_centers_use_dg1.pkl", "rb") as f:
        first_cluster_centers: Dict = pickle.load(f)
    first_cluster_centers.pop("4")
    with open("../data/raw/clusters_centers_use_dg2.pkl", "rb") as f:
        second_cluster_centers = pickle.load(f)

    print(first_cluster_centers.keys())
    print(second_cluster_centers.keys())

    second_cluster_centers = keymap(lambda x: str(int(x) + len(first_cluster_centers)), second_cluster_centers)
    second_cluster_texts = keymap(lambda x: str(int(x) + len(first_cluster_texts)), second_cluster_texts)

    print("AFTER:")

    print(first_cluster_texts.keys())
    print(second_cluster_texts.keys())

    cluster_centers = {
        **first_cluster_centers,
        **second_cluster_centers
    }
    print(cluster_centers.keys())
    with open("../data/preprocessed/clusters_centers.pkl", "wb") as f:
        pickle.dump(cluster_centers, f)

    with open("../data/preprocessed/clusters_use_dg1.json", "w") as f:
        json.dump(first_cluster_texts, f)
    with open("../data/preprocessed/clusters_use_dg2.json", "w") as f:
        json.dump(second_cluster_texts, f)

    with open("../data/preprocessed/service_mappings.json", "w") as f:
        json.dump({
            **{int(k): 0 for k in first_cluster_texts.keys()},
            **{int(k): 1 for k in  second_cluster_texts.keys()}
        }, f)

    print(f"Copying data to {output_dir}")
    fs = get_s3_file_system()
    fs.put("../data/preprocessed/", f"{output_dir}/", recursive=True)


if __name__ == "__main__":
    main()
